# gromozeka1980-caterpillarlogic
- Essayez le jeu Caterpillar Logic disponible pour Android sur [Google Play][1]
- Avez-vous avez compris le principe ? 
- Ce jeu a été développé en Python pour Android avec la plateforme Kivy. Vous trouverez les sources sur [Github][2]
- Le but de ce projet est d'en développer une version *en mode texte*
- Parmi les bibliothèques qui peuvent être utiles pour un tel développement,  on peut citer 
    [curses][3] qui permet d'afficher des caractères n'importe où sur une fenêtre en mode texte
	
- C'est aussi l'occasion de créer vos propres niveaux, en remplacement ou en complément [de ceux existants][5].

[1]: https://play.google.com/store/apps/details?id=org.gromozeka1980.caterpillar_logic
[2]: https://github.com/gromozeka1980/kivy_contest_2014/tree/master/caterpillars
[3]: https://docs.python.org/fr/3/library/curses.html#module-curses
[4]: https://pypi.org/project/colorama/
[5]: https://github.com/gromozeka1980/kivy_contest_2014/blob/master/caterpillars/rules.py
